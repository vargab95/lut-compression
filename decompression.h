#ifndef __DECOMPRESSION_H__
#define __DECOMPRESSION_H__

#include "common.h"

process_return_code_t decompress(processing_request_t *request);

#endif
