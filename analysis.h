#ifndef __ANALYSIS_H__
#define __ANALYSIS_H__

#include "common.h"

process_return_code_t analyze(processing_request_t *request);

#endif
