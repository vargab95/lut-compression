#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

typedef uint8_t processing_unit_t;
#define PROCESSING_UNIT_BYTES (sizeof(processing_unit_t))
#define PROCESSING_UNIT_BITS (PROCESSING_UNIT_BYTES * 8)
#define PROCESSING_UNIT_VALUE_COUNT (256)

typedef enum
{
    WRONG_PARAMETER = -1,
    OK = 0,
    MEMORY_ERROR = 1,
    FILE_CANNOT_BE_OPENED = 2,
    HELP_REQUESTED = 3,
    IO_ERROR = 4
} process_return_code_t;

typedef struct
{
    uint8_t archive_version;
    uint8_t compression_window_size;
    uint8_t reserved_character_byte;
    uint8_t compressed_sequence_idx_byte;
    uint8_t lut_size;
} compression_configuration_t;

typedef enum {
    UNKNOWN_MODE = 0,
    COMPRESSION_MODE = 1,
    DECOMPRESSION_MODE = 2,
    ANALYSIS_MODE = 3
} operation_mode_t;

typedef struct
{
    operation_mode_t mode;
    bool verbose;
    bool progress;
    char *input_path;
    FILE *input_file;
    char *output_path;
    FILE *output_file;
    uint32_t reference_counting_buffer_size;
    compression_configuration_t configuration;
} processing_request_t;

#endif
