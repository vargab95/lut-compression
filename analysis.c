#include "analysis.h"
#include "reference_tree.h"

process_return_code_t analyze(processing_request_t *request)
{
    process_return_code_t return_code;
    reference_tree_t reference_tree = {{0}};

    return_code = count_references(request, &reference_tree);
    if (return_code == OK)
    {
        print_reference_tree(&reference_tree);
    }

    return return_code;
}
