#include <stdlib.h>
#include <string.h>

#include "configuration.h"
#include "ranking.h"
#include "reference_tree.h"

#include "compression.h"

static process_return_code_t write_archive(processing_request_t *request, reference_ranking_element_t *ranking);

process_return_code_t compress(processing_request_t *request)
{
    process_return_code_t return_code;
    reference_tree_t reference_tree = {{0}};
    reference_ranking_element_t *ranking = NULL;

    return_code = count_references(request, &reference_tree);
    if (return_code == OK)
    {
        if (request->verbose)
        {
            print_reference_tree(&reference_tree);
        }
    }

    if (return_code == OK)
    {
        fill_compression_configuration(&reference_tree, &request->configuration);

        if (request->verbose)
        {
            print_compression_configuration(&request->configuration);
        }

        return_code = fill_highest_reward_sequences(&request->configuration, &reference_tree, &ranking);
    }

    if (return_code == OK && request->verbose)
    {
        print_ranking(&request->configuration, ranking);
    }

    if (return_code == OK)
    {
        return_code = write_archive(request, ranking);
    }

    if (request->output_path)
    {
        fclose(request->output_file);
    }

    return return_code;
}

static process_return_code_t write_archive(processing_request_t *request, reference_ranking_element_t *ranking)
{
    reference_ranking_element_t *current;
    processing_unit_t *read_buffer;
    uint32_t lut_length = 0;

    fwrite(&request->configuration, sizeof(compression_configuration_t), 1, request->output_file);
    fwrite(&lut_length, sizeof(lut_length), 1, request->output_file);

    current = ranking;
    for (int i = 0; i < request->configuration.lut_size && current; ++i, current = current->next)
    {
        lut_length += fwrite(current->sequence, sizeof(processing_unit_t),
                             current->sequence_length + sizeof(processing_unit_t), request->output_file);
    }

    fseek(request->output_file, sizeof(compression_configuration_t), SEEK_SET);
    fwrite(&lut_length, sizeof(lut_length), 1, request->output_file);
    fseek(request->output_file, 0, SEEK_END);

    read_buffer = calloc(sizeof(processing_unit_t), request->reference_counting_buffer_size);
    fseek(request->input_file, 0, SEEK_SET);

    uint64_t read_bytes;
    while ((read_bytes = fread(read_buffer, sizeof(processing_unit_t), request->reference_counting_buffer_size,
                               request->input_file)) > 0)
    {
        for (uint32_t i = 0; i < read_bytes;)
        { // TODO handle wrapping points
            char byte = read_buffer[i];
            if (byte == request->configuration.reserved_character_byte ||
                byte == request->configuration.compressed_sequence_idx_byte)
            {
                char reserved_buffer[2];

                reserved_buffer[0] = request->configuration.reserved_character_byte;
                reserved_buffer[1] = byte;

                fwrite(reserved_buffer, sizeof(char), 2, request->output_file);
                ++i;

                continue;
            }

            bool found = false;
            current = ranking;
            for (int j = 0; j < request->configuration.lut_size && current; ++j, current = current->next)
            {
                if (memcmp(&read_buffer[i], current->sequence, current->sequence_length) == 0)
                {
                    char sequence_buffer[2];

                    sequence_buffer[0] = request->configuration.compressed_sequence_idx_byte;
                    sequence_buffer[1] = j;

                    fwrite(sequence_buffer, sizeof(char), 2, request->output_file);
                    found = true;

                    i += current->sequence_length;

                    break;
                }
            }

            if (found)
            {
                continue;
            }

            fwrite(&read_buffer[i], sizeof(char), 1, request->output_file);
            ++i;
        }
    }

    free(read_buffer);

    return OK;
}
