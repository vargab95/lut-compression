#include <stdlib.h>

#include "common.h"
#include "decompression.h"

typedef struct
{
    processing_unit_t *value;
    uint32_t length;
} lut_element_t;

process_return_code_t decompress(processing_request_t *request)
{
    process_return_code_t return_code = OK;
    uint32_t lut_length;
    lut_element_t *lut;
    processing_unit_t *buffer, sequence;

    if (fread(&request->configuration, sizeof(compression_configuration_t), 1, request->input_file) != 1)
    {
        return IO_ERROR;
    }

    lut = calloc(sizeof(lut_element_t), request->configuration.lut_size);
    if (lut == NULL)
    {
        return MEMORY_ERROR;
    }

    if (fread(&lut_length, sizeof(lut_length), 1, request->input_file) != 1)
    {
        return_code = IO_ERROR;
    }

    buffer = NULL;
    if (return_code == OK)
    {
        buffer = malloc(lut_length);
        if (lut == NULL)
        {
            return_code = MEMORY_ERROR;
        }
    }

    if (return_code == OK)
    {
        if (fread(buffer, lut_length, 1, request->input_file) != 1)
        {
            return_code = IO_ERROR;
        }
    }

    if (return_code == OK)
    {
        uint32_t processed_size = 0;
        uint32_t lut_id = 0;
        uint32_t i = 0;

        while (i < lut_length / sizeof(processing_unit_t))
        {
            lut_element_t *element = &lut[lut_id];

            element->value = &buffer[i];
            element->length = 0;

            while (element->value[element->length])
            {
                element->length++;
                ++i;
            }

            ++lut_id;
            ++i;
        }
    }

    if (request->verbose)
    {
        puts("Look up table:");
        for (int i = 0; i < request->configuration.lut_size; ++i)
        {
            lut_element_t *element = &lut[i];

            printf("%4d  %4d  ", i, element->length);

            for (int j = 0; j < element->length; ++j)
            {
                printf(" %02x", element->value[j]);
            }

            putchar('\n');
        }
    }

    while (return_code == OK && fread(&sequence, sizeof(processing_unit_t), 1, request->input_file) == 1)
    {
        if (request->configuration.compressed_sequence_idx_byte == sequence)
        {
            processing_unit_t lut_id;
            if (fread(&lut_id, sizeof(processing_unit_t), 1, request->input_file) == 1)
            {
                if (fwrite(lut[lut_id].value, lut[lut_id].length, 1, request->output_file) != 1)
                {
                    return_code = IO_ERROR;
                }
            }
            else
            {
                return_code = IO_ERROR;
            }
        }
        else if (request->configuration.reserved_character_byte == sequence)
        {
            if (fread(&sequence, sizeof(processing_unit_t), 1, request->input_file) == 1)
            {
                if (fwrite(&sequence, sizeof(processing_unit_t), 1, request->output_file) != 1)
                {
                    return_code = IO_ERROR;
                }
            }
            else
            {
                return_code = IO_ERROR;
            }
        }
        else
        {
            if (fwrite(&sequence, sizeof(processing_unit_t), 1, request->output_file) != 1)
            {
                return_code = IO_ERROR;
            }
        }
    }

    if (buffer)
        free(buffer);
    free(lut);

    return return_code;
}
