#include <string.h>
#include <unistd.h>

#include "common.h"
#include "configuration.h"

#define ONE_GIGABYTE (1073741824)

process_return_code_t process_command_line_arguments(int argc, char **argv, processing_request_t *request)
{
    int opt;
    int tmp;
    process_return_code_t return_code = OK;

    memset(request, 0, sizeof(processing_request_t));

    request->configuration.archive_version = 1;
    request->configuration.compression_window_size = DEFAULT_COMPRESSION_WINDOW_SIZE;
    request->reference_counting_buffer_size = DEFAULT_REFERENCE_COUNTING_BUFFER_SIZE;
    request->configuration.lut_size = DEFAULT_LUT_SIZE;

    while ((opt = getopt(argc, argv, ":cdai:o:vw:r:l:hp")) != -1 && return_code == OK)
    {
        switch (opt)
        {
        case 'h':
            return_code = HELP_REQUESTED;
            break;
        case 'c':
            request->mode = COMPRESSION_MODE;
            break;
        case 'd':
            request->mode = DECOMPRESSION_MODE;
            break;
        case 'a':
            request->mode = ANALYSIS_MODE;
            break;
        case 'i':
            request->input_path = optarg;
            break;
        case 'o':
            request->output_path = optarg;
            break;
        case 'v':
            request->verbose = true;
            break;
        case 'p':
            request->progress = true;
            break;
        case 'w':
            if (sscanf(optarg, "%d", &tmp) != 1)
            {
                request->configuration.compression_window_size = DEFAULT_COMPRESSION_WINDOW_SIZE;
            }
            else
            {
                if (tmp < 0 || tmp > 255)
                {
                    return_code = WRONG_PARAMETER;
                }

                request->configuration.compression_window_size = tmp;
            }
            break;
        case 'r':
            if (sscanf(optarg, "%d", &tmp) != 1)
            {
                request->reference_counting_buffer_size = DEFAULT_REFERENCE_COUNTING_BUFFER_SIZE;
            }
            else
            {
                if (tmp < 0 || tmp > ONE_GIGABYTE)
                {
                    return_code = WRONG_PARAMETER;
                }

                request->reference_counting_buffer_size = tmp;
            }
            break;
        case 'l':
            if (sscanf(optarg, "%d", &tmp) != 1)
            {
                request->configuration.lut_size = DEFAULT_LUT_SIZE;
            }
            else
            {
                if (tmp < 0 || tmp > 255)
                {
                    return_code = WRONG_PARAMETER;
                }

                request->configuration.lut_size = tmp;
            }
            break;
        case ':':
            printf("Option %c needs a value\n", opt);
            return_code = WRONG_PARAMETER;
        case '?':
            printf("Unknown option: %c\n", optopt);
            return_code = WRONG_PARAMETER;
        }
    }

    if (return_code != HELP_REQUESTED && request->mode == UNKNOWN_MODE)
    {
        return_code = WRONG_PARAMETER;
    }

    if (request->input_path == NULL)
    {
        request->input_file = stdin;
    }

    if (request->output_path == NULL)
    {
        request->output_file = stdout;
    }

    if (return_code != OK)
    {
        printf("%s parameters:\n"
               "    -c - Compression mode\n"
               "    -d - Decompression mode\n"
               "    -a - Analysis mode\n"
               "    -i - Input file path\n"
               "    -o - Ouput file path\n"
               "    -w - Compression window size\n"
               "    -r - Refernce counting window size\n"
               "    -l - LUT size\n"
               "    -v - Verbose mode\n"
               "    -p - Print progress bar\n"
               "    -h - Help menu\n",
               argv[0]);
    }

    return return_code;
}

void fill_compression_configuration(reference_tree_t *tree, compression_configuration_t *configuration)
{
    const uint64_t min_default = 0xFFFFFFFFFFFFFFFF;
    uint64_t min = min_default;
    processing_unit_t byte;
    reference_tree_element_t *iterator;
    uint64_t reference_counts[PROCESSING_UNIT_VALUE_COUNT] = {0};

    iterator = &tree->root;
    while (iterator)
    {
        if (iterator->reference_count < min)
        {
            reference_counts[iterator->byte] = iterator->reference_count;
        }
        iterator = iterator->next;
    }

    for (int i = PROCESSING_UNIT_VALUE_COUNT - 1; i >= 0; --i)
    {
        if (reference_counts[i] == 0)
        {
            configuration->compressed_sequence_idx_byte = i;
            byte = i;
            break;
        }

        if (reference_counts[i] < min)
        {
            configuration->compressed_sequence_idx_byte = reference_counts[i];
            byte = i;
        }
    }

    reference_counts[byte] = min_default;

    for (int i = PROCESSING_UNIT_VALUE_COUNT - 1; i >= 0; --i)
    {
        if (reference_counts[i] == 0)
        {
            configuration->reserved_character_byte = i;
            byte = i;
            break;
        }

        if (reference_counts[i] < min)
        {
            configuration->reserved_character_byte = reference_counts[i];
            byte = i;
        }
    }
}

void print_compression_configuration(compression_configuration_t *configuration)
{
    printf("Configuration:\n"
           "  Compression window size:        %d\n"
           "  Compressed sequence index byte: %x\n"
           "  Reserved character byte:        %x\n"
           "  LUT size:                       %d\n",
           configuration->compression_window_size, configuration->compressed_sequence_idx_byte,
           configuration->reserved_character_byte, configuration->lut_size);
}
