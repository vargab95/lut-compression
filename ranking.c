#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "ranking.h"

static process_return_code_t _fill_highest_reward_sequences_inner(compression_configuration_t *configuration,
                                                                  reference_tree_element_t *element,
                                                                  reference_ranking_element_t **ranking, uint8_t depth);
static void get_text(reference_tree_element_t *tree, processing_unit_t *buffer, uint32_t depth, uint32_t level);

process_return_code_t fill_highest_reward_sequences(compression_configuration_t *configuration,
                                                    reference_tree_t *reference_tree,
                                                    reference_ranking_element_t **ranking)
{
    return _fill_highest_reward_sequences_inner(configuration, &reference_tree->root, ranking, 1);
}

static process_return_code_t _fill_highest_reward_sequences_inner(compression_configuration_t *configuration,
                                                                  reference_tree_element_t *element,
                                                                  reference_ranking_element_t **ranking, uint8_t depth)
{
    process_return_code_t return_code;
    processing_unit_t buffer[256 * sizeof(processing_unit_t)];

    if (element == NULL)
    {
        return OK;
    }

    if (depth > 2 && element->reference_count >= 0)
    {
        if (*ranking == NULL)
        {
            *ranking = malloc(sizeof(reference_ranking_element_t));
            if (*ranking == NULL)
            {
                return MEMORY_ERROR;
            }

            (*ranking)->tree_element = element;
            (*ranking)->depth = depth;
            (*ranking)->next = NULL;
            (*ranking)->no_spared_bytes = depth * element->reference_count * PROCESSING_UNIT_BYTES;

            get_text(element, buffer, depth, 0);
            (*ranking)->sequence_length = depth * sizeof(processing_unit_t);
            (*ranking)->sequence = calloc(depth, sizeof(processing_unit_t));
            memcpy((*ranking)->sequence, buffer, (*ranking)->sequence_length);
        }
        else
        {
            reference_ranking_element_t *prev = NULL, *current = *ranking;
            uint32_t position = 0;
            while (current && position < configuration->lut_size)
            {
                if (current->no_spared_bytes < depth * element->reference_count * PROCESSING_UNIT_BYTES)
                {
                    break;
                }

                prev = current;
                current = current->next;

                ++position;
            }

            if (position < configuration->lut_size)
            {
                if (prev == NULL)
                {
                    *ranking = malloc(sizeof(reference_ranking_element_t));
                    if (*ranking == NULL)
                    {
                        return MEMORY_ERROR;
                    }

                    (*ranking)->tree_element = element;
                    (*ranking)->depth = depth;
                    (*ranking)->no_spared_bytes = depth * element->reference_count * PROCESSING_UNIT_BYTES;

                    (*ranking)->next = current;
                    current->next = (current->next) ? current->next->next : NULL;

                    get_text(element, buffer, depth, 0);
                    (*ranking)->sequence_length = depth * sizeof(processing_unit_t);
                    (*ranking)->sequence = calloc(depth, sizeof(processing_unit_t));
                    memcpy((*ranking)->sequence, buffer, (*ranking)->sequence_length);
                }
                else
                {
                    prev->next = malloc(sizeof(reference_ranking_element_t));
                    if (prev->next == NULL)
                    {
                        return MEMORY_ERROR;
                    }

                    prev->next->tree_element = element;
                    prev->next->depth = depth;
                    prev->next->no_spared_bytes = depth * element->reference_count * PROCESSING_UNIT_BYTES;
                    prev->next->next = current;

                    get_text(element, buffer, depth, 0);
                    prev->next->sequence_length = depth * sizeof(processing_unit_t);
                    prev->next->sequence = calloc(depth, sizeof(processing_unit_t));
                    memcpy(prev->next->sequence, buffer, prev->next->sequence_length);
                }
            }
        }
    }

    return_code = _fill_highest_reward_sequences_inner(configuration, element->children, ranking, depth + 1);
    if (return_code != OK)
    {
        return return_code;
    }

    return _fill_highest_reward_sequences_inner(configuration, element->next, ranking, depth);
}

void print_ranking(compression_configuration_t *configuration, reference_ranking_element_t *ranking)
{
    reference_ranking_element_t *current;
    int i;

    for (i = 0, current = ranking; current && i < configuration->lut_size; current = current->next, ++i)
    {
        printf("%4d%20ld %4d  ", i, current->no_spared_bytes, current->sequence_length);

        for (int j = 0; current->sequence[j]; ++j)
        {
            printf(" %02x", current->sequence[j]);
        }

        putchar('\n');
    }
}

static void get_text(reference_tree_element_t *tree, processing_unit_t *buffer, uint32_t depth, uint32_t level)
{
    if (tree->parent)
    {
        get_text(tree->parent, buffer, depth, level + 1);
    }

    buffer[depth - level - 1] = tree->byte;
    buffer[depth] = '\0';
}
