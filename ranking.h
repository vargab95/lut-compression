#ifndef __RANKING_H__
#define __RANKING_H__

#include <stdbool.h>

#include "common.h"
#include "reference_tree.h"

typedef struct reference_ranking_element_t
{
    uint64_t no_spared_bytes;
    uint8_t depth;
    processing_unit_t *sequence;
    uint16_t sequence_length;
    reference_tree_element_t *tree_element;
    struct reference_ranking_element_t *next;
} reference_ranking_element_t;

process_return_code_t fill_highest_reward_sequences(compression_configuration_t *configuration,
                                                    reference_tree_t *reference_tree,
                                                    reference_ranking_element_t **ranking);
void print_ranking(compression_configuration_t *configuration, reference_ranking_element_t *ranking);

#endif
