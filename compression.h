#ifndef __COMPRESSION_H__
#define __COMPRESSION_H__

#include "common.h"

process_return_code_t compress(processing_request_t *request);

#endif
