#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <stdbool.h>

#include "common.h"
#include "reference_tree.h"

#define COMPRESS_MODE_SWITCH "-c"
#define EXTRACT_MODE_SWITCH "-x"

#define DEFAULT_REFERENCE_COUNTING_BUFFER_SIZE (1048576)
#define DEFAULT_COMPRESSION_WINDOW_SIZE (8)
#define DEFAULT_LUT_SIZE (16)

process_return_code_t process_command_line_arguments(int argc, char **argv, processing_request_t *request);
void fill_compression_configuration(reference_tree_t *tree, compression_configuration_t *configuration);
void print_compression_configuration(compression_configuration_t *configuration);

#endif
