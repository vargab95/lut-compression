#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "analysis.h"
#include "common.h"
#include "compression.h"
#include "configuration.h"
#include "decompression.h"

int main(int argc, char **argv)
{
    process_return_code_t return_code = OK;
    processing_request_t request;

    return_code = process_command_line_arguments(argc, argv, &request);
    if (return_code != OK)
    {
        if (return_code == HELP_REQUESTED)
        {
            return OK;
        }

        return return_code;
    }

    if (!request.input_file)
    {
        request.input_file = fopen(request.input_path, "r");
        if (request.input_file == NULL)
        {
            return FILE_CANNOT_BE_OPENED;
        }
    }

    if (!request.output_file && request.mode != ANALYSIS_MODE)
    {
        request.output_file = fopen(request.output_path, "w");
        if (request.output_file == NULL)
        {
            return_code = FILE_CANNOT_BE_OPENED;
        }
    }

    switch (request.mode)
    {
    case ANALYSIS_MODE:
        return_code = analyze(&request);
        break;
    case COMPRESSION_MODE:
        return_code = compress(&request);
        break;
    case DECOMPRESSION_MODE:
        return_code = decompress(&request);
        break;
    case UNKNOWN_MODE:
        fprintf(stderr, "Invalid execution mode\n");
        break;
    }

    if (request.input_path)
    {
        fclose(request.input_file);
    }

    return return_code;
}
