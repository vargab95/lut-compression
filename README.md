# LUT Compression

A simple look up table based compression algorithm implemented
for fun and to compare the efficiency of a simple, brute force
approach to more advanced ones, like LZ77 or Huffman tree based
compression.

# Algorithm

1. Count references of character sequences with a maximum length
   of a predefined window size
2. Rank sequences based on the their length * reference count
3. Select the N sequences with the highest reward
4. Write an archive header with the configuration and the LUT
5. Write the archive body and replace sequences with LUT ids
   where possible
