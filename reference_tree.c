#include <stdlib.h>

#include "reference_tree.h"

#define ONE_MEGABYTE (1048576)

static reference_tree_element_t *allocate_element(reference_tree_t *tree);

process_return_code_t count_references(processing_request_t *request, reference_tree_t *reference_tree)
{
    processing_unit_t *buffer;
    uint64_t read_bytes, all_bytes = 0, file_size, number_of_elements = 0, allocated_for_elements = 0;
    processing_unit_t *start;
    reference_tree_element_t *element, *prev;

    buffer = calloc(PROCESSING_UNIT_BYTES, request->reference_counting_buffer_size);

    if (request->progress)
    {
        puts("Reference counting");

        fseek(request->input_file, 0, SEEK_END);
        file_size = ftell(request->input_file);
        fseek(request->input_file, 0, SEEK_SET);
    }

    while ((read_bytes =
                fread(buffer, PROCESSING_UNIT_BYTES, request->reference_counting_buffer_size, request->input_file)) > 0)
    {
        for (uint32_t i = 0; i < read_bytes && i < request->reference_counting_buffer_size -
                                                       request->configuration.compression_window_size;
             ++i, all_bytes++)
        {
            if (request->progress && (all_bytes % (ONE_MEGABYTE / 10) == 0))
            {
                printf("\r%02.2f %%  %ld of %ld bytes", (float)all_bytes / file_size * 100.0, all_bytes, file_size);

                if (request->verbose)
                {
                    printf("  Number of elements: %ld (%ld bytes)", number_of_elements, allocated_for_elements);
                }

                fflush(stdout);
            }

            start = &buffer[i];
            element = &reference_tree->root;
            prev = NULL;

            for (uint32_t j = 0; j < request->configuration.compression_window_size && j < (read_bytes - i); ++j)
            {
                if (j > 0)
                {
                    if (element->children == NULL)
                    {
                        element->children = allocate_element(reference_tree);
                        if (element->children == NULL)
                        {
                            return MEMORY_ERROR;
                        }

                        ++number_of_elements;
                        allocated_for_elements += sizeof(reference_tree_element_t);

                        element->children->byte = start[j];
                        element->children->parent = element;
                    }
                    element = element->children;
                }

                // printf("%-3x", start[j]);
                while (element && element->byte != start[j])
                {
                    prev = element;
                    element = element->next;
                }

                if (element == NULL)
                {
                    element = prev->next = allocate_element(reference_tree);
                    if (element == NULL)
                    {
                        return MEMORY_ERROR;
                    }

                    ++number_of_elements;
                    allocated_for_elements += sizeof(reference_tree_element_t);

                    element->byte = start[j];
                    element->parent = prev->parent;
                }

                element->reference_count++;
            }

            ++start;

            // putchar('\n');
        }

        // printf("New chunk at %ld\n", all_bytes);

        if (read_bytes > request->configuration.compression_window_size)
        {
            fseek(request->input_file, -request->configuration.compression_window_size, SEEK_CUR);
            all_bytes -= request->configuration.compression_window_size;
        }
    }

    if (request->progress)
    {
        putchar('\n');
    }

    return OK;
}

void print_reference_tree(reference_tree_t *reference_tree)
{
    print_reference_tree_element(&reference_tree->root, 0);
}

void print_reference_tree_element(reference_tree_element_t *element, uint8_t indent)
{
    if (element == NULL)
    {
        return;
    }

    if (element->reference_count != 0)
    {
        printf("%*c%02x%*ld\n", -indent - 1, '\r', element->byte, 40 - indent, element->reference_count);
        print_reference_tree_element(element->children, indent + 2);
    }

    print_reference_tree_element(element->next, indent);
}

static reference_tree_element_t *allocate_element(reference_tree_t *tree)
{
    reference_tree_element_t *element;
    reference_tree_memory_page_t *first_page;

    first_page = tree->pages;

    if (!tree->pages || tree->pages->occuppied >= tree->pages->size)
    {
        tree->pages = calloc(1, sizeof(reference_tree_memory_page_t));
        if (tree->pages == NULL)
        {
            return NULL;
        }

        tree->pages->next = first_page;

        tree->pages->size = 4000000;
        tree->pages->elements = calloc(tree->pages->size, sizeof(reference_tree_element_t));
        if (tree->pages->elements == NULL)
        {
            return NULL;
        }
    }

    element = &tree->pages->elements[tree->pages->occuppied];
    tree->pages->occuppied++;

    return element;
}
