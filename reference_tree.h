#ifndef __REFERENCE_TREE_H__
#define __REFERENCE_TREE_H__

#include "common.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct reference_tree_element_t
{
    uint64_t reference_count;
    uint8_t byte;
    struct reference_tree_element_t *parent;
    struct reference_tree_element_t *children;
    struct reference_tree_element_t *next;
} reference_tree_element_t;

typedef struct reference_tree_memory_page_t
{
    uint32_t occuppied;
    uint32_t size;
    reference_tree_element_t *elements;
    struct reference_tree_memory_page_t *next;
} reference_tree_memory_page_t;

typedef struct
{
    reference_tree_element_t root;
    reference_tree_memory_page_t *pages;
} reference_tree_t;

process_return_code_t count_references(processing_request_t *request, reference_tree_t *reference_tree);
void print_reference_tree(reference_tree_t *reference_tree);
void print_reference_tree_element(reference_tree_element_t *element, uint8_t indent);

#endif
